import StudentDetails from './components/StudentDetails.vue'
import Students from './components/Students.vue'
import CreateStudent from './components/student/CreateStudent.vue'

export const routes = [
    {path : '*', redirect: {name:'students'}},
    {path : '/', component:Students, name: 'students'},
    {path : '/studentDetails/:student_id', component:StudentDetails,
     name: 'studentDetails',
     props: true},
    {path : '/addStudent', component:CreateStudent, name: 'addStudent'},
];

