<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'address_id';

    public function student()
    {
        return $this->belongsTo('App\Student','student_id');
    }
}
