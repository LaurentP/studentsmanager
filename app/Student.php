<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $primaryKey = 'student_id';
    public function phones()
    {
        return $this->hasMany('App\Phone', 'student_id');
    }
    public function emails()
    {
        return $this->hasMany('App\Email', 'student_id');
    }
    public function addresses()
    {
        return $this->hasMany('App\Address', 'student_id');
    }
}
