<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->string('email',100)->primary(); // VARCHAR and Primary key
            $table->integer('student_id')->unsigned(); // UNSIGNED INT
            $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade')->onUpdate('cascade');//Foreign Key
            $table->enum('email_type', ['professional', 'personal']); // ENUM
            $table->boolean('display')->default(true); //BOOLEAN
            $table->timestamps(); // DATETIME (created_at and updated_at)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
