## Students Manager
A little project to manage a list of students ( REST app ).

## Front-end
Vue.js

## Back-end
Laravel

## Database
Mysql ( see the file SchemaOfStudentsManagerDatabase.pdf to visualize its schema )


## Installation

<ol>
    <li>
        <h4> Download the archive</h4>
    </li>
    <li>
        <h4>Open the project root directory</h4>
        <p>cd folderName</p>
    </li>
    <li>
        <h4>Rename .env.example file to .env inside your project root and fill the database information</h4>
        <ul>
            <li>DB_DATABASE=studentsManager</li>
            <li>DB_USERNAME=root</li>
            <li>DB_PASSWORD='' </li>
        </ul>
    </li>
    <li>
        <h4>Install the dependencies</h4>
        <ul>
            <li>npm install</li>
            <li>composer install </li>
        </ul>
    </li>
    <li>
        <h4>Create a dataBase named studentsManager inside MySql</h4>
    </li>
    <li>
        <h4>Generate an encryption key</h4>
        <p>php artisan key:generate</p>
    </li>
    <li>
        <h4>Migrate the tables in the dataBase</h4>
        <p>php artisan migrate</p>
    </li>    
    <li>
        <h4>Create some randoms rows</h4>
        <p>php artisan db:seed</p>
    </li>   
    <li>
        <h4>Run the server</h4>
        <p>php artisan serve</p>
    </li>       
    <li>
        <h4>In your browser go to localhost:8000</h4>
    </li>      
</ol>
