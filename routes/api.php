<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// -------- Students --------:
// List the students
Route::get('students','StudentController@index');

// List all the student with their phones
Route::get('studentsWithPhones','StudentController@indexStudentWithPhones');

// List a single student
Route::get('student/{student_id}','StudentController@show');

// Hide a single student and all his elements
Route::put('student/hide/{student_id}','StudentController@hide');

// Create a student
Route::post('student','StudentController@store');

// Update a student
Route::put('student','StudentController@store');

// Delete a student
Route::delete('student/{student_id}','StudentController@destroy');

// -------- Addresses --------:
// List all the addresses
Route::get('addresses','AddressController@index');

// List all the addresses of a student
Route::get('student/{student_id}/addresses','AddressController@indexForStudent');

// List one address
Route::get('address/{address_id}','AddressController@show');

// Hide a single address
Route::put('address/hide/{address_id}','AddressController@hide');

// Create a address
Route::post('address','AddressController@store');

// Update a address
Route::put('address','AddressController@store');

// Delete a address
Route::delete('address/{address_id}','AddressController@destroy');

// -------- Emails --------:
// List all the Emails
Route::get('emails','EmailController@index');

// List the emails of a student
Route::get('student/{student_id}/emails','EmailController@indexForStudent');

// List one email
Route::get('email/{email_id}','EmailController@show');

// Hide a single address
Route::put('email/hide/{email_id}','EmailController@hide');

// Create an email
Route::post('email','EmailController@store');

// Update an email
Route::put('email','EmailController@store');

// Delete an email
Route::delete('email/{email}','EmailController@destroy');

// -------- Phones --------:
// List all the phones
Route::get('phones','PhoneController@index');

// List all the phones of a student
Route::get('student/{student_id}/phones','PhoneController@indexForStudent');

// List one phone
Route::get('phone/{phone_id}','PhoneController@show');

// Hide a single address
Route::put('phone/hide/{address_id}','PhoneController@hide');

// Create a phone
Route::post('phone','PhoneController@store');

// Update a phone
Route::put('phone','PhoneController@store');

// Delete a phone
Route::delete('phone/{phone_id}','PhoneController@destroy');